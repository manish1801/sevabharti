jQuery( function() {
    jQuery("#edit-sd").datepicker();
    jQuery("#edit-ed").datepicker();
    jQuery("#edit-sd").css({
        'max-width': '224px'
    });
    jQuery("#edit-ed").css({
        'max-width': '224px'
    });
    jQuery(".form--inline").css({
        'display': 'flex',
        'align-items': 'flex-end'
    });
    jQuery("#edit-submit-transaction-details").css({
        'margin-bottom': '-3px'
    });
});