jQuery( function() {
    jQuery("#edit-sd").datepicker();
    jQuery("#edit-ed").datepicker();
    jQuery("#edit-sd").css({
        'max-width': '224px'
    });
    jQuery("#edit-ed").css({
        'max-width': '224px'
    });
    jQuery(".form--inline").css({
        'display': 'flex',
        'align-items': 'flex-end'
    });
    jQuery("#edit-actions").css({
        'margin-bottom': '10px'
    });
    jQuery("#print-patient-visit-sheet").click(printTransactions);

    var sd = getParameterByName('sd');
    var ed = getParameterByName('ed');

    console.log(sd);
    console.log(ed);

    var header_txt = "";

    if(sd !== null && sd !== ""){

        sd = new Date(sd);
        var month = parseInt(sd.getMonth().toString())+1;
        sd = sd.getDate() + "/" + month + "/" + sd.getFullYear();

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        today = dd + '/' + mm + '/' + yyyy;

        if(ed !== null && ed !== ""){
            ed = new Date(ed);
            month = parseInt(ed.getMonth().toString())+1;
            ed = ed.getDate() + "/" + month + "/" + ed.getFullYear();
            header_txt = "<h4>From : "+sd+" to : "+ed+" </h4>";
        }
        if(ed === null || ed === ""){
            header_txt = "<h4>From : "+sd+" to : "+today+"</h4>";
        }
    }



    jQuery(".view-header").prepend(header_txt);

});

function printTransactions() {

    var data = jQuery('.view-content').html();
    var header = jQuery('.view-header').html();

    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    //var css_url = "/modules/custom/charges_slip/assets/css/patient-visit-sheet-print.css";

    mywindow.document.write('<html><head><title></title>');
    //mywindow.document.write('<link href="'+css_url+'" rel="stylesheet" media="print">');
    var c = "*{font-size:12px;font-family:Consolas,monaco,monospace}table,td,th{border:1px solid #fff!important;text-align:left!important}table{border-collapse:collapse!important;width:100%!important}thead>tr{border-bottom:2px dashed #cacaca}td,th{padding-right:1px;padding-left:1px}tfoot>tr>td{padding:0;font-weight:700;border-top:2px dashed #cacaca!important}tr td:nth-child(8),tr th:nth-child(8){text-align:right!important}tr td:nth-child(9),tr th:nth-child(9){display:none!important}";
    mywindow.document.write('<style>'+c+'</style>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>Seva Bharti New Alipore Trust - Patient Visit Sheet</h1>');
    var d = new Date();
    var m = Number(d.getMonth())+1;
    var date = d.getDate() + "-" + m + "-" + d.getFullYear() + " , " + d.getHours() + ":" + d.getMinutes();
    mywindow.document.write('<h2 style="text-align: right">Date : '+date+'</h2>');
    mywindow.document.write('<p>'+header+'</p>');
    mywindow.document.write('<div>'+data+'</div>');
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    },500);

    return true;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}