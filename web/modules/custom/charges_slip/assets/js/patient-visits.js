jQuery(function () {
    jQuery("#edit-sd").datepicker();
    jQuery("#edit-ed").datepicker();
    jQuery("#edit-field-transaction-target-id-value").remove();
    jQuery("label[for='edit-op']").html("Has Transaction");

    jQuery("#edit-op").css({
        'min-width': '120px'
    });
    jQuery("#edit-sd").css({
        'max-width': '224px'
    });
    jQuery("#edit-ed").css({
        'max-width': '224px'
    });
    jQuery(".form--inline").css({
        'display': 'flex',
        'align-items': 'flex-end'
    });
    jQuery("#edit-submit-patient-visits").css({
        'margin-bottom': '-3px'
    });

    var options = "<option value=\"!=\"> - Any - </option><option value=\"empty\"> No </option><option value=\"not empty\"> Yes </option>";

    jQuery("#edit-op").html(options);

    var op = getParameterByName("op");

    if (op) {
        jQuery("#edit-op").val(op);
    }
    else {
        jQuery("#edit-op").val("!=");
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    jQuery("#print-patient-visits").css({
        'border': '1px solid #dedede',
        'padding': '8px',
        'display': 'inline-block',
        'width': '40px',
        'text-align': 'center',
        'background-color': '#dedede',
        'color': '#111',
        'border-radius': '4px',
        'cursor': 'pointer'
    });

    jQuery("#print-patient-visits").click(printTransactions);

    function printTransactions() {
        var data = jQuery('.view-content').html();
        console.log(data);

        var mywindow = window.open('', 'PRINT', 'height=600,width=800');

        var css_url = '/modules/custom/charges_slip/assets/css/patient-visits-print.css';

        mywindow.document.write('<html><head><title></title>');
        mywindow.document.write('<link href="' + css_url + '" rel="stylesheet" media="print">');
        mywindow.document.write('</head><body >');
        mywindow.document.write('<div>' + data + '</div>');
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        setTimeout(function () {
            mywindow.print();
            mywindow.close();
        }, 500);

        var b = 0;
        return true;

    }

});