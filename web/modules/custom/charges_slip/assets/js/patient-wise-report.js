jQuery(function() {
    let amt = parseInt(jQuery("tfoot>tr>#view-field-amount-table-column").text());
    let paid = parseInt(jQuery("tfoot>tr>#view-field-cash-paid-table-column").text());
    let due = amt - paid;
    jQuery("#due-calc").append(due);
    jQuery("#due-calc").css("text-align","right");

    jQuery("table tbody").append(jQuery("table tfoot>tr"));
    jQuery("table tfoot").remove();

    jQuery("#print-patient-wise-sheet").click(printPtientReport);
});

function printPtientReport() {


    var data = jQuery('.view-content').html();
    var header = jQuery('.view-header').html();
    var due = jQuery('#due-calc').html();

    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    //var css_url = "/modules/custom/charges_slip/assets/css/patient-visit-sheet-print.css";

    mywindow.document.write('<html><head><title></title>');
    //mywindow.document.write('<link href="'+css_url+'" rel="stylesheet" media="print">');
    var c = "*{font-size:12px;font-family:Consolas,monaco,monospace}table,td,th{border:1px solid #fff!important;text-align:left!important}table{border-collapse:collapse!important;width:100%!important}thead>tr{border-bottom:2px dashed #cacaca}tfoot>tr{border-top:2px dashed #cacaca}td,th{padding-right:1px;padding-left:1px}tr td:nth-child(9),tr th:nth-child(9){text-align:right!important}#due-calc{text-align:right}";
    mywindow.document.write('<style>'+c+'</style>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h2>Seva Bharti New Alipore Trust - Patient Report</h2>');
    var d = new Date();
    var m = Number(d.getMonth())+1;
    var date = d.getDate() + "-" + m + "-" + d.getFullYear() + " , " + d.getHours() + ":" + d.getMinutes();
    mywindow.document.write('<h2 style="text-align: right">Date : '+date+'</h2>');
    mywindow.document.write(header);
    mywindow.document.write('<br><br>');
    mywindow.document.write('<div>'+data+'</div>');
    mywindow.document.write('<div id="due-calc">'+due+'</div>');
    mywindow.document.write('</body></html>');

    mywindow.document.close();
    mywindow.focus();


    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    },500);

    return true;
}