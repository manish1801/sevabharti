jQuery(document).ready(function () {

    jQuery("#print-dues").click(printDues);

    jQuery("#edit-sd").datepicker();
    jQuery("#edit-ed").datepicker();
    jQuery("#edit-sd").css({
        'max-width': '224px'
    });
    jQuery("#edit-ed").css({
        'max-width': '224px'
    });

});

function printDues() {

    var data = jQuery('.view-content').html();
    console.log(data);

    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    mywindow.document.write('<html><head><title></title>');
    var c = "*{font-size:12px;font-family:Consolas,monaco,monospace}table,td,th{border:1px solid #fff;text-align:left}table{border-collapse:collapse;width:100%}td,th{padding:15px 1px}tr td:nth-child(5),tr th:nth-child(5){display:none}tbody>tr>td{padding-top:1px;padding-bottom:1px}thead>tr>th{padding:0;border-bottom:2px dashed #cacaca}tfoot>tr>td{padding:0;font-weight:700;border-top:2px dashed #cacaca}tr td:nth-child(4),tr th:nth-child(4){text-align:right}";
    mywindow.document.write('<style>'+c+'</style>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h2>Seva Bharti New Alipore Trust - Payment Dues</h2>');
    var d = new Date();
    var m = Number(d.getMonth())+1;
    var date = d.getDate() + "-" + m + "-" + d.getFullYear() + " , " + d.getHours() + ":" + d.getMinutes();
    mywindow.document.write('<h2 style="text-align: right">Date : '+date+'</h2>');
    mywindow.document.write('<div>'+data+'</div>');
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    },500);

    return true;
}