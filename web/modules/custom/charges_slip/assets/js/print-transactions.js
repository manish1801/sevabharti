jQuery(document).ready(function () {

    var op = '<option value="All" selected="selected">- Any -</option><option value="1">Yes</option><option value="0">No</option>';

    jQuery('#edit-slip-generated').html(op);

    var slipGenerated = getParameterByName('slip-generated');

    if (slipGenerated !== null)
        jQuery('#edit-slip-generated').val(slipGenerated);

    var sd = getParameterByName('sd');

    jQuery("#print-transactions").click(printTransactions);

    function printTransactions() {

        var data = jQuery('.view-content').html();

        var mywindow = window.open('', 'PRINT', 'height=600,width=800');

        var css_url = "/modules/custom/charges_slip/assets/css/transactions-print.css";

        mywindow.document.write('<html><head><title></title>');
        // mywindow.document.write('<link href="'+css_url+'" rel="stylesheet" media="print">');
        var c = "*{font-size:12px;font-family: Consolas, monaco, monospace;}table,td,th{border:1px solid #fff;text-align:left}table{border-collapse:collapse;width:100%}td,th{padding:15px}tr td:nth-child(8),tr th:nth-child(8){display:none}td,th{padding-right:1px;padding-left:1px}tbody>tr>td{padding-top:1px;padding-bottom:1px}thead>tr>th{padding:0;border-bottom:2px dashed #cacaca}tfoot>tr>td{padding:0;font-weight:700;border-top:2px dashed #cacaca}td,th{text-align:right}";
        mywindow.document.write('<style>'+c+'</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write('<h2>Seva Bharti New Alipore Trust - Transactions</h2>');
        var d = new Date();
        var m = Number(d.getMonth())+1;
        var date = d.getDate() + "-" + m + "-" + d.getFullYear() + " , " + d.getHours() + ":" + d.getMinutes();
        mywindow.document.write('<h2 style="text-align: right">Date : '+date+'</h2>');
        mywindow.document.write('<div>'+data+'</div>');
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        setTimeout(function () {
            mywindow.print();
            mywindow.close();
        },500);

        var b = 0;
        return true;
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});