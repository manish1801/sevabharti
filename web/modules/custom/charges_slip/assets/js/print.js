/**
 * Created by monoit on 17/1/18.
 */
function print_slip() {
    var css_url = '/modules/custom/charges_slip/assets/css/dortor-consultant-print.css';
    var mywindow = window.open('', 'PRINT', 'height=600,width=800');

    mywindow.document.write('<html><head><title></title>');
    // mywindow.document.write('<link href="'+css_url+'" rel="stylesheet" media="print">');
    var c = "body,h1,h2,h3,h4,h5,h6,hr{margin:0}.sb--fontweight-100,body{font-weight:100}body{border:0;padding:0;max-width:100vw;font-family:calibri,sans-serif;font-size:16px;-webkit-font-smoothing:antialiased}*,::after,::before{box-sizing:border-box;outline:0}:focus{outline:0}::selection{color:rgba(33,150,243,1)}.sb--text-uppercase{text-transform:uppercase}.sb--text-center{text-align:center}.sb--text-right{text-align:right}.sb--container{margin:0 auto;width:1024px}.sb--receiptinfo-container{display:flex;flex-flow:row nowrap;justify-content:space-evenly;align-items:center;align-content:center}.sb--receiptinfo-items{margin:4px;flex-basis:341px;font-size:10px}.sb--display-inline-block{display:inline-block}.sb--width-100{width:100%}.sb--margin-8{margin:8px}.sb--margin-10{margin:10px}.sb--m-t-80{margin-top:80px}.sb--m-l-0{margin-left:0}.sb--m-l-20{margin-left:20px}.sb--m-r-0{margin-right:0}.custom-header{display:flex;justify-content:space-between}.custom-header>.sb--receiptinfo-items{flex-basis:auto;margin:4px}.custom-height{max-height:24px}.custom-font{font-size:12px}*{font-size:10px;font-family: Consolas, monaco, monospace;}";
    mywindow.document.write('<style>'+c+'</style>');
    mywindow.document.write('</head><body style="margin-top: 80px">');
    mywindow.document.write(document.getElementById("doctor-consultant-receipt").innerHTML);
    // mywindow.document.write("<br/><br/>");
    // mywindow.document.write(document.getElementById("doctor-consultant-receipt").innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    setTimeout(function () {
        mywindow.print();
        mywindow.close();
    },500);

    var b = 0;
    return true;
}