var service_count = 0;
var update_clicked = false;
jQuery(document).ready(function () {

    setCurrentDate();
    getDoctorsList();

    jQuery.get("get-patient-codes", function (data, status) {
        jQuery("#patient_code").autocomplete({
            source: data,
            select: function () {
                setTimeout(function () {
                    var id = jQuery("#patient_code").val();
                    id = id.substring(0, id.indexOf("-") - 1);
                    jQuery("#patient_code").val(id);
                }, 5);
                setTimeout(function () {
                    getPatient();
                }, 10);
            }
        });
    });

    jQuery('#doctor_amount').val(0);
    jQuery('#old_due_amount').val(0);
    jQuery('#total').val(0);
    jQuery('#cash_paid').val(0);
    jQuery('#due').val(0);

    jQuery("#info-button").click(getPatient);
    jQuery("#doctor_name").change(getDepartment);
    jQuery("#doctor_amount").change(getTotalAmount);
    jQuery("#cash_paid").blur(getDue);
    jQuery("#add-more").click(add_more_service);

    // jQuery("#generate_slip").change(function () {
    //     if (jQuery("#generate_slip").is(":checked")) {
    //         jQuery('#receipt_no_wrapper').css({display: 'none'});
    //         jQuery('#receipt_no').val("");
    //     }
    //     else {
    //         jQuery('#receipt_no_wrapper').css({display: 'block'});
    //         var old_due_amount = parseInt(jQuery("#old_due_amount").val());
    //         jQuery("#total").val(old_due_amount);
    //     }
    // });

});

var services = {};
var i = 0;

function setCurrentDate() {
    var now = new Date();
    var month = (now.getMonth() + 1);
    var day = now.getDate();
    if (month < 10)
        month = "0" + month;
    if (day < 10)
        day = "0" + day;
    var today = now.getFullYear() + '-' + month + '-' + day;
    jQuery('#date').val(today);
}

function getPatient() {
    var id = jQuery("#patient_code").val();
    jQuery.get("get-patient-details?id=" + id, function (data, status) {

        if (data['patient_id'] == null) {
            alert('Invalid Patient Code');

            jQuery('#patient_name').removeAttr('readonly');
            jQuery('#patient-id').removeAttr('readonly');
            jQuery('#phone_mobile').removeAttr('readonly');
            jQuery('#age').removeAttr('readonly');
            jQuery('#sex').removeAttr('disabled');
            jQuery('#patient_address').removeAttr('readonly');
            jQuery('#old_due_amount').removeAttr('readonly');
            jQuery('#old_due_amount').removeAttr('readonly');

            jQuery('#patient-id').val('');
            jQuery('#patient_name').val('');
            jQuery('#patient_code').val('');
            jQuery('#phone_mobile').val('');
            jQuery('#age').val('');
            jQuery('#sex').val('');
            jQuery('#patient_address').val('');
            jQuery('#old_due_amount').val(0);
        }
        else {
            jQuery('#patient_name').val(data['patient_name']);
            jQuery('#patient_name').attr('readonly', 'readonly');
            jQuery('#patient-id').val(data['patient_id']);
            jQuery('#patient-id').attr('readonly', 'readonly');
            jQuery('#phone_mobile').val(data['phone_mobile']);
            jQuery('#phone_mobile').attr('readonly', 'readonly');
            jQuery('#age').val(data['age']);
            jQuery('#age').attr('readonly', 'readonly');
            jQuery('#sex').val(data['sex']);
            jQuery('#sex').attr('disabled', true);
            jQuery('#patient_address').val(data['patient_address']);
            jQuery('#patient_address').attr('readonly', 'readonly');

            if (data['old_due_amount'] == null) {
                jQuery('#old_due_amount').val(0);
                jQuery('#old_due_amount').attr('readonly', 'readonly');
            }
            else {
                jQuery('#old_due_amount').val(data['old_due_amount']);
                jQuery('#old_due_amount').attr('readonly', 'readonly');
            }
        }
        getTotalAmount();
    });
}

function getDepartment() {
    var id = jQuery("#doctor_name").val();

    if (id == '') {
        jQuery('#add-more').attr("disabled", "disabled");
        jQuery('.service-list').remove();
        jQuery('#doctor_amount').val('0');
        jQuery('#doctor_amount').change();
    }
    else
        jQuery('#add-more').removeAttr("disabled");

    jQuery.get("get-department?id=" + id, function (data, status) {
        jQuery('#department').val(data);
        get_total_service_amount();
    });

    //jQuery("#dname").val(id);

    // jQuery("#doctor_name").css({'background-color':'#ebebe4'});
    // jQuery("#doctor_name").attr('disabled',true);


}

function getDoctorsList() {

    jQuery.get("get-doctors-list", function (data, status) {
        var optionsAsString = "";
        jQuery.map(data, function (item, index) {
            optionsAsString += "<option value='" + index + "'>" + item + "</option>";
        });
        jQuery('select[name="doctor_name"]').append(optionsAsString);

    });
}

var c = 0;

function getServiceList() {

    jQuery.get("get-service-list", function (data, status) {
        var optionsAsString = "";
        jQuery.map(data, function (item, index) {
            optionsAsString += "<option value='" + index + "'>" + item + "</option>";
        });
        jQuery('select[name="service"]').append(optionsAsString);
        jQuery('select[name="service"]').attr("name", "service" + ++c);

    });
}

function getTotalAmount() {

    var old;
    var doc;

    if (jQuery('#old_due_amount').val().length === 0)
        old = 0;
    else
        old = parseInt(jQuery('#old_due_amount').val());

    if (jQuery('#doctor_amount').val().length === 0)
        doc = 0;
    else
        doc = parseInt(jQuery('#doctor_amount').val());

    var t = old + doc;

    jQuery('#total').val(t);

    getDue();

}

function getDue() {
    var t = parseInt(jQuery('#total').val()) - parseInt(jQuery('#cash_paid').val());
    if(t >= 0)
        jQuery('#due').val(t);
    else
        jQuery('#due').val('0');
}

var sc = 0;
var sid = 0;
function add_more_service() {
    service_count++;
    var string = '<div class="form-group"><select name="service" id="service'+(++sid)+'" class="services service-list" required onchange="get_total_service_amount('+(sid)+');"> <option value="" selected>Select</option> </select> </div>';
    jQuery("#service").append(string);
    getServiceList();

    var count_option = "<input type='hidden' id='sid-"+(++sc)+"' value='0' /><input type='hidden' id='sa-"+(sc)+"' value='0' /><input type='number' class='service-count' id='sc-"+(sc)+"' name='sc-"+(sc)+"' onchange='changeCount("+(sc)+")' value='1' min='1'/>";
    jQuery("#service-count").append(count_option);

    jQuery("#service-action").append("<input type='button' value='Remove' class='remove-buttons' id='rm-"+sc+"' onclick='removeService(event,"+sc+")'>");

    jQuery("#service-base-amount").append("<input type='number' class='service-amounts' value='0' id='service-base-amount-"+sc+"' onchange='changeCount("+(sc)+")'>");

    jQuery("#service-amount").append("<input type='number' class='service-amounts' value='0' id='service-amount-"+sc+"' readonly tabindex='-1'>");

    if(service_count === 1){
        jQuery("#rm-1").attr('disabled','disabled');
        jQuery("#rm-1").css('opacity','0.5');
    }

}

// services = [];
i = 0;

function get_total_service_amount(sc) {
    var service_id = jQuery("#service"+sc).val();
    var doctor_id = jQuery("#doctor_name").val();


    var option_text = jQuery("#service"+sc+" option:selected").text();

    if(option_text){

        if (option_text !== 'Select') {

            if (option_text !== 'Doctor Consultant') {

                jQuery.get("get-service-amount?type=medi_test&doctor_id=" + doctor_id + "&service_id=" + service_id, function (data, status) {

                    jQuery("#sid-"+sc).val(service_id);
                    jQuery("#sa-"+sc).val(data['amount']);
                    jQuery("#service-base-amount-"+sc).val(data['amount']);
                    jQuery("#service-amount-"+sc).val(data['amount']);
                    jQuery("#sc-"+sc).val(1);

                    // services[sc-1] = {};
                    // services[sc-1].service_id = service_id;
                    // services[sc-1].amount = parseInt(data['amount']);
                    i++;
                    // jQuery("#select_service").val(JSON.stringify(services));


                });

            }

            else {

                jQuery.get("get-service-amount?type=consultant&doctor_id=" + doctor_id + "&service_id=" + service_id, function (data, status) {

                    jQuery("#sid-"+sc).val(service_id);
                    jQuery("#sa-"+sc).val(data['amount']);
                    jQuery("#service-base-amount-"+sc).val(data['amount']);
                    jQuery("#service-amount-"+sc).val(data['amount']);
                    jQuery("#sc-"+sc).val(1);

                    // services[sc-1] = {};
                    // services[sc-1].service_id = service_id;
                    // services[sc-1].amount = parseInt(data['amount']);
                    i++;
                    // jQuery("#select_service").val(JSON.stringify(services));

                });

            }

        }

    }


    // jQuery(".services").each(function () {
    //     var option_text = jQuery("option:selected", this).text();
    //     if (option_text != 'Select') {
    //         var service_id = jQuery(this).val();
    //         if (option_text != 'Doctor Consultant') {
    //             ///total_amount += this.value;
    //             jQuery.get("get-service-amount?type=medi_test&doctor_id=" + doctor_id + "&service_id=" + service_id, function (data, status) {
    //
    //                 jQuery("#sa-"+sc).val(data['amount']);
    //                 //jQuery("#sid-"+sc).val(service_id);
    //
    //                 total_amount += parseInt(data['amount']);
    //                 //jQuery("#doctor_amount").val(total_amount);
    //                 var old_due_amount = parseInt(jQuery("#old_due_amount").val());
    //
    //                 if (jQuery("#generate_slip").is(":checked")) {
    //                     //jQuery("#total").val(total_amount + old_due_amount);
    //                     //getDue();
    //                 }
    //                 else {
    //                     //jQuery("#total").val(old_due_amount);
    //                     //getDue();
    //                 }
    //                 services[i] = {};
    //                 services[i].service_id = service_id;
    //                 services[i].amount = parseInt(data['amount']);
    //                 i++;
    //                 jQuery("#select_service").val(JSON.stringify(services));
    //             });
    //             var a = 0;
    //         }
    //         else {
    //             jQuery.get("get-service-amount?type=consultant&doctor_id=" + doctor_id + "&service_id=" + service_id, function (data, status) {
    //
    //                 jQuery("#sa-"+sc).val(data['amount']);
    //                 //jQuery("#sid-"+sc).val(service_id);
    //
    //                 total_amount += parseInt(data['amount']);
    //                 //jQuery("#doctor_amount").val(total_amount);
    //                 var old_due_amount = parseInt(jQuery("#old_due_amount").val());
    //                 //jQuery("#total").val(total_amount + old_due_amount);
    //                 getDue();
    //                 services[i] = {};
    //                 services[i].service_id = service_id;
    //                 services[i].amount = parseInt(data['amount']);
    //                 i++;
    //                 jQuery("#select_service").val(JSON.stringify(services));
    //             });
    //         }
    //     }
    // });

}

function updateAmount() {

    update_clicked = true;

    setTimeout(function () {
        if(service_count > 0){

            var total = 0;
            var services = [];

            for(var j=1;j<=service_count;j++){

                if(Object.keys(jQuery("#sa-"+j)).length > 0){

                    var amt = parseInt(jQuery("#service-amount-"+j).val());
                    var base_amt = parseInt(jQuery("#service-base-amount-"+j).val());
                    var cn = parseInt(jQuery("#sc-"+j).val());
                    // var count = parseInt(jQuery("#sc-"+j).val());
                    // total = total + (amt*count);
                    total = total + amt;

                    services[j-1] = {};
                    services[j-1].service_id = parseInt(jQuery("#sid-"+j).val());
                    services[j-1].base_amount = base_amt;
                    services[j-1].count = cn;
                    services[j-1].amount = amt;


                }

            }

            jQuery("#doctor_amount").val(total);

            var old_due_amount = parseInt(jQuery("#old_due_amount").val());
            // if (jQuery("#generate_slip").is(":checked")) {
            //     jQuery("#total").val(total + old_due_amount);
            //     getDue();
            // }
            // else {
            //     jQuery("#total").val(old_due_amount);
            //     getDue();
            // }
            jQuery("#total").val(total + old_due_amount);
            getDue();

            jQuery("#select_service").val(JSON.stringify(services));

        }
    },1000);

}

function removeService(e,sc) {
    e.preventDefault();

    if (sc>1){

        jQuery("#sid-"+sc).remove();
        jQuery("#sa-"+sc).remove();
        jQuery("#service-base-amount-"+sc).remove();
        jQuery("#service-amount-"+sc).remove();
        jQuery("#service"+sc).remove();
        jQuery("#sc-"+sc).remove();
        jQuery("#rm-"+sc).remove();
        // services[sc-1] = {};
        // jQuery("#select_service").val(JSON.stringify(services));

    }


}

function changeCount(sc) {
    var single_amt = parseInt(jQuery("#service-base-amount-"+sc).val());
    var count = parseInt(jQuery("#sc-"+sc).val());
    var new_amt = single_amt * count;
    jQuery("#service-amount-"+sc).val(new_amt);
}

function submit_form(form_id) {

    //var total_amount = 0;
    //var doctor_id = jQuery("#doctor_name").val();
    /*jQuery(".services").each(function () {
        var option_text = jQuery("option:selected", this).text();
        if(option_text != 'Select') {
            if (option_text != 'Doctor Consultant') {
                ///total_amount += this.value;
                var service_id = jQuery(this).val();
                jQuery.get("get-service-amount?type=medi_test&doctor_id=" + doctor_id + "&service_id=" + service_id, function (data, status) {
                    total_amount = parseInt(data['amount']);
                    services[i] = {};
                    services[i].service_id = service_id;
                    services[i].amount     = parseInt(data['amount']);
                    i++;
                    jQuery("#select_service").val(JSON.stringify(services));
                });
                var a = 0;
            }
            else {
                jQuery.get("get-service-amount?type=consultant&doctor_id=" + doctor_id + "&service_id=" + service_id, function (data, status) {
                    total_amount = parseInt(data['amount']);
                    services[i] = {};
                    services[i].service_id = service_id;
                    services[i].amount     = parseInt(data['amount']);
                    i++;
                    jQuery("#select_service").val(JSON.stringify(services));
                });
            }
        }
    });*/

    let pc = jQuery('#patient_code').val();
    let pn = jQuery('#patient_name').val();
    let dp = jQuery('#department').val();
    let dn = jQuery('#doctor_name').val();
    //let sr = jQuery('#service1').val();
    // let sv = jQuery('#service-main').val() === ""?null:jQuery('#service-main').val();
    if(pc && pn && dp && dn && update_clicked){
        jQuery(".login-loader").addClass("is-active");
        jQuery("#" + form_id).submit();
    }else {
        pc?"":alert("Patient Code Empty");
        pn?"":alert("Patient Name Empty");
        dp?"":alert("Department Empty");
        dn?"":alert("Service Name Empty");
        update_clicked?"":alert("Amounts not Updated");
        //sr?"":alert("No Service Selected");
        // sv?"":alert("Services Empty");
    }

    //jQuery("#" + form_id).submit();
}