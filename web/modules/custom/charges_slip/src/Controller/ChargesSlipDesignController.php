<?php

namespace Drupal\charges_slip\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ChargesSlipDesignController.
 */
class ChargesSlipDesignController extends ControllerBase
{

    public function slipDesign()
    {

        $build = [
            '#theme' => 'slip_design',
            '#attached' => [
                'library' => [
                    'charges_slip/slip-styles',
                ]
            ]
        ];

        $build['#cache']['max-age'] = 0;

        return $build;
    }

    public function getPatientDetails()
    {

        $patient = array();

        $pcode = \Drupal::request()->query->get('id');

        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'patient')
            ->condition('field_patient_code', $pcode);
        $entity_ids = $query->execute();
        $pid = reset($entity_ids);

        $patientDetails = Node::load($pid);

        if ($patientDetails != NULL) {

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_patient', $pid)
                ->condition('field_status', 'u');
            $entity_ids = $query->execute();

            $did = reset($entity_ids);
            $due = Node::load($did);

            $patient['old_due_amount'] = $due->field_amount_due->value;
            $patient['patient_name'] = $patientDetails->getTitle();
            $patient['patient_address'] = $patientDetails->field_address->value;
            $patient['phone_mobile'] = $patientDetails->field_phone_number->value;
            $patient['age'] = $patientDetails->field_age->value;
            $patient['sex'] = $patientDetails->field_sex->value;
            $patient['patient_id'] = $pid;

        }

        return new JsonResponse($patient);
    }

    public function getDepartment()
    {
        $did = \Drupal::request()->query->get('id');
        $doc = Node::load($did);
        $deptId = $doc->field_department->target_id;
        $term = Term::load($deptId);
        $dept = $term->getName();
        return new JsonResponse($dept);
    }

    public function getDoctorsList()
    {

        $doc = array();

        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'doctor');
        $dids = $query->execute();

        foreach ($dids as $did) {
            $d = Node::load($did);
            $nm = $d->getTitle();
            $doc[$did] = $nm;
        }

        return new JsonResponse($doc);
    }

    public function getServiceList()
    {

        $properties['name'] = 'service';
        $terms = \Drupal::service('entity_type.manager')
            ->getStorage("taxonomy_term")
            ->loadTree('service', $parent = 0, $max_depth = NULL, $load_entities = FALSE);

        $services = array();

        for ($i = 0; $i < sizeof($terms); $i++) {
            $services[$terms[$i]->tid] = $terms[$i]->name;
        }

        return new JsonResponse($services);
    }

    public function generateTransaction()
    {

        $patientId = $_POST['patient_id'];
        $patientDetails = Node::load($patientId);

        $pid = -1;

        if ($patientDetails == NULL) {

            $patientName = $_POST['patient_name'];
            $patientAdd = $_POST['patient_address'];
            $patientPh = $_POST['phone_mobile'];
            $patientAge = $_POST['age'];
            $patientSex = $_POST['sex'];
            $patientCode = $_POST['patient_code'];

            $nids = \Drupal::entityQuery('node')->condition('type', 'patient')->execute();

            $flag = 1;
            foreach ($nids as $key => $value) {
                $node = Node::load($key);
                $pc = $node->field_patient_code->value;
                if ($pc == $patientCode) {
                    $flag = 0;
                    break;
                }
            }

            if ($flag) {
                $node = Node::create([
                    'type' => 'patient',
                    'title' => $patientName,
                    'field_address' => $patientAdd,
                    'field_age' => $patientAge,
                    'field_phone_number' => $patientPh,
                    'field_sex' => $patientSex,
                    'field_patient_code' => $patientCode,
                ]);
                $node->save();
                $pid = $node->id();
                drupal_set_message('Patient created Successfully');
            } else {
                drupal_set_message('Patient Code Already Exists', 'error');
                $response = new RedirectResponse('/slip');
                $response->send();
                return;
            }


        }

        if ($pid == -1)
            $pid = $_POST['patient_id'];

        $tdate = explode(' ', DrupalDateTime::createFromTimestamp(time()));

        $node = Node::create([
            'type' => 'transaction',
            'title' => 'Sample Transaction',
            'field_prescription_number' => $_POST['prescription_no'],
            'field_doctor' => $_POST['doctor_name'],
            'field_patient' => $pid,
            'field_cash_paid' => $_POST['cash_paid'],
            'field_transaction_type' => 'Doctor\'s Consultant',
            'field_transaction_date' => $tdate,
            'field_narration' => $_POST['narration'],
            'field_blood_pressure' => $_POST['bp'],
            'field_blood_sugar_fasting' => $_POST['bs_fs'],
            'field_blood_sugar_pp' => $_POST['bs_pp'],
            'field_blood_sugar_random' => $_POST['bs_rn'],
        ]);
        $node->save();
        $tid = $node->id();

        $node = Node::load((int)$node->id());
        $field = FieldConfig::load('node.transaction.field_sl_no');
        $serialStorage = \Drupal::getContainer()->get('serial.sql_storage');
        $serial = $serialStorage->generateValue($field, $node);
        $node->set('field_sl_no', $serial);

        if($_POST['cash_paid'] > 0){

            $field2 = FieldConfig::load('node.transaction.field_receipt_no');
            $serialStorage2 = \Drupal::getContainer()->get('serial.sql_storage');
            $serial2 = $serialStorage2->generateValue($field2, $node);
            $node->set('field_receipt_no', $serial2);
            $node->set('field_slip_generated', 1);

        }
        else{
            $node->set('field_slip_generated', 0);
        }

//        if (isset($_POST['generate_slip'])) {
//
//            $field2 = FieldConfig::load('node.transaction.field_receipt_no');
//            $serialStorage2 = \Drupal::getContainer()->get('serial.sql_storage');
//            $serial2 = $serialStorage2->generateValue($field2, $node);
//            $node->set('field_receipt_no', $serial2);
//            $node->set('field_slip_generated', 1);
//
//        } else {
//            $node->set('field_receipt_no', $_POST['receipt_no']);
//            $node->set('field_slip_generated', 0);
//        }

        $node->save();

        $tid = $node->id();

        if ($_POST['due'] > 0) {

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_patient', $pid)
                ->condition('field_status', 'u');
            $entity_ids = $query->execute();

            if (sizeof($entity_ids) > 0) {
                $did = reset($entity_ids);
                $due = Node::load($did);
                $due->field_status->value = 'p';
                $due->save();
                $node = Node::create([
                    'type' => 'payment_due',
                    'field_amount_due' => $_POST['due'],
                    'field_patient' => $pid,
                    'field_transaction' => $tid,
                ]);
                $node->save();
                drupal_set_message('Payment Due Added');
            } else {
                $node = Node::create([
                    'type' => 'payment_due',
                    'field_amount_due' => $_POST['due'],
                    'field_patient' => $pid,
                    'field_status' => 'u',
                    'field_transaction' => $tid,
                ]);
                $node->save();
                drupal_set_message('Payment Due Added');
            }
        }

        if ($_POST['due'] == "" || $_POST['due'] == "0") {


            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_patient', $pid)
                ->condition('field_status', 'u');
            $entity_ids = $query->execute();

            if (sizeof($entity_ids) > 0) {
                $did = reset($entity_ids);
                $due = Node::load($did);
                $due->field_status->value = 'p';
                $due->save();
                drupal_set_message('Payment Due Cleared');
            }
        }

        $total = 0;
        if ($_POST['select_service'] != '') {
            $select_services = json_decode($_POST['select_service'], 1);

            foreach ($select_services as $select_service) {

                if(sizeof($select_service)>0){

                    $node = Node::create([
                        'type' => 'transaction_service',
                        'title' => 'Sample Transaction',
                        'field_service' => $select_service['service_id'],
                        'field_transaction' => $tid,
                        'field_base_amount' => $select_service['base_amount'],
                        'field_service_count' => $select_service['count'],
                        'field_amount' => $select_service['amount'],
                    ]);
                    $node->save();
                    $total += $select_service['amount'];

                }
            }
        }
        $node = Node::load($tid);
        $node->set("field_amount",$total);
        $node->save();

        drupal_set_message('Transaction Created Successfully');
        $response = new RedirectResponse('/print-doctor-consultant?id=' . $tid);
        $response->send();

    }

    public function getServiceAmount()
    {
        $service_id = \Drupal::request()->query->get('service_id');
        $doctor_id = \Drupal::request()->query->get('doctor_id');
        $type = \Drupal::request()->query->get('type');
        $amount = 0;
        if ($type != 'consultant') {
            $service = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($service_id);
            //$amount   = $service->amount->value;
            $service = $service->toArray();
            $amount = $service['field_amount'][0]['value'];
        } else {
            $doctor = Node::load($doctor_id);
            $doctor = $doctor->toArray();

            $department_id = $doctor['field_department'][0]['target_id'];
            $department = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($department_id);
            $department = $department->toArray();

            $amount = $department['field_amount'][0]['value'];
        }
        return new JsonResponse(array('amount' => $amount));
    }

    public function doctorWise()
    {

        $data = array();

        $fromDate = \Drupal::request()->query->get('fromDate');
        $toDate = \Drupal::request()->query->get('toDate');

        $data['fromDate'] = $fromDate;
        $data['toDate'] = $toDate;

        if ($toDate == '') {
            $date = explode(' ', DrupalDateTime::createFromTimestamp(time()));
            $toDate = $date[0];
        }
        if ($fromDate == '') {
            $date = explode(' ', DrupalDateTime::createFromTimestamp(time()));
            $fromDate = $date[0];
        }

        $view = \Drupal\views\Views::getView('doctor_wise_patient_list');
        $view->setDisplay('default');
        $filters = $view->display_handler->getOption('filters');
        if ($filters['field_transaction_date_value_1']) {
            foreach ($filters['field_transaction_date_value_1']['value'] as $key => $filter) {
                $filters['field_transaction_date_value_1']['value']['min'] = $fromDate;
                $filters['field_transaction_date_value_1']['value']['max'] = $toDate;
            }
        }
        $view->display_handler->overrideOption('filters', $filters);
        $view->execute();
        $result = $view->result;
        $reports = array();
        $total_amount = 0;
        $total_patient = 0;
        $transaction_query = \Drupal::entityQuery('node');
        $transaction_query->condition('status', 1);
        $transaction_query->condition('type', 'transaction');
        if ($fromDate != '') {
            $transaction_query->condition('field_transaction_date', $fromDate, '>=');
        }
        $transaction_query->condition('field_transaction_date', $toDate, '<=');

        $transaction_ids = $transaction_query->execute();

        $first = reset($transaction_ids);
        $last = end($transaction_ids);

        $first_transaction_node = Node::load($first);
        $last_transaction_node = Node::load($last);
        $first_sl_no = $first_transaction_node->field_sl_no->value;
        $first_receipt_no = $first_transaction_node->field_receipt_no->value;
        $last_sl_no = $last_transaction_node->field_sl_no->value;
        $last_receipt_no = $last_transaction_node->field_receipt_no->value;

        foreach($result as $key => $value) {
            $reports[$key]['department'] = $value->taxonomy_term_field_data_node__field_department_name;
            $reports[$key]['doctor'] = $value->node_field_data_node__field_doctor_title;
            $reports[$key]['patient'] = $value->node__field_patient_field_patient_target_id;
            $reports[$key]['amount'] = $value->node__field_cash_paid_field_cash_paid_value;
            $total_amount = $total_amount + $value->node__field_cash_paid_field_cash_paid_value;
            $total_patient = $total_patient + $value->node__field_patient_field_patient_target_id;

        }
        $data['transactions'] = $reports;
        $data['total_amt'] = $total_amount;
        $data['total_patient'] = $total_patient;
        $data['first_sl_no'] = $first_sl_no;
        $data['last_sl_no'] = $last_sl_no;
        $data['first_receipt_no'] = $first_receipt_no;
        $data['last_receipt_no'] = $last_receipt_no;

        /*$transaction_query = \Drupal::entityQuery('node');
        $transaction_query->condition('status', 1);
        $transaction_query->condition('type', 'transaction');
        if ($fromDate != '') {
            $transaction_query->condition('field_transaction_date', $fromDate, '>=');
        }
        $transaction_query->condition('field_transaction_date', $toDate, '<=');
        //$transaction_query->range(0, 2000);
        $transaction_ids = $transaction_query->execute();

        $f=0;
        $first_sl_no = 0;
        $last_sl_no = 0;
        $first_receipt_no = 0;
        $last_receipt_no = 0;
        $doctors = array();
        $transactions = array();
        foreach ($transaction_ids as $transaction_id) {

            $transaction = array();
            $transaction_node = Node::load($transaction_id);

            if($f==0){
                $first_sl_no = $transaction_node->field_sl_no->value;
                $first_receipt_no = $transaction_node->field_receipt_no->value;
                $f=1;
            }
            else{
                $last_sl_no = $transaction_node->field_sl_no->value;
                $last_receipt_no = $transaction_node->field_receipt_no->value;
            }

            $doc_id = $transaction_node->field_doctor->target_id;
            $doc = Node::load($doc_id);
            $deptId = $doc->field_department->target_id;
            $dept = Term::load($deptId);
            $doc_name = $doc->getTitle();
            $dept_name = $dept->getName();
            $cash_paid = $transaction_node->field_cash_paid->value;
            $transaction['doc_name'] = $doc_name;
            $transaction['dept_name'] = $dept_name;
            $transaction['cash_paid'] = $cash_paid;

            array_push($transactions,$transaction);

            if(!in_array(['name'=>$doc_name,'dept'=>$dept_name],$doctors)){
                $d = array();
                $d['name'] = $doc_name;
                $d['dept'] = $dept_name;
                array_push($doctors,$d);
            }
        }

        $reports = array();
        foreach ($doctors as $doctor){
            $report = array();
            $p = 0;
            $c = 0;
            foreach ($transactions as $transaction){
                if($transaction['doc_name'] == $doctor['name']){
                    $p++;
                    $c = $c + $transaction['cash_paid'];
                }
            }
            $report['doctor'] = $doctor['name'];
            $report['department'] = $doctor['dept'];
            $report['patient'] = $p;
            $report['amount'] = $c;
            array_push($reports,$report);
        }

        $total_amt = 0;
        $total_p = 0;
        foreach ($reports as $report) {
            $total_amt += $report['amount'];
            $total_p += $report['patient'];
        }*/

        /*$data['transactions'] = $reports;
        $data['total_amt'] = $total_amt;
        $data['total_patient'] = $total_p;
        $data['first_sl_no'] = $first_sl_no;
        $data['last_sl_no'] = $last_sl_no;
        $data['first_receipt_no'] = $first_receipt_no;
        $data['last_receipt_no'] = $last_receipt_no;*/

        $build = [
            '#theme' => 'doctor_wise',
            '#data' => $data,
            '#attached' => [
                'library' => [
                    'charges_slip/sevabharti-doctor-wise',
                ]
            ]
        ];

        $build['#cache']['max-age'] = 0;

        return $build;
    }

    public function getPatientCodes()
    {

        $pcodes = array();
        $nids = \Drupal::entityQuery('node')->condition('type', 'patient')->execute();
        foreach ($nids as $key => $value) {
            $node = Node::load($key);
            $str = $node->field_patient_code->value . ' - ' . $node->getTitle();
            array_push($pcodes, $str);
        }

        return new JsonResponse($pcodes);
    }

    public function sb_access(){

        $roles = \Drupal::currentUser()->getRoles();
        if (in_array("administrator", $roles) || in_array("seva_bharti_administrator", $roles)) {
            return AccessResult::allowed();
        }
        return AccessResult::forbidden();

    }

}
