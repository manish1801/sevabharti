<?php

namespace Drupal\charges_slip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use function PHPSTORM_META\type;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class EditTransactionController.
 */
class EditTransactionController extends ControllerBase
{

    public function editPermissionValidate()
    {
        $data = array();
        $nid = \Drupal::request()->query->get('nid');
        $data['nid'] = $nid;

        $transaction_node = Node::load($nid);
        $t_date = $transaction_node->field_transaction_date->value;

        $today = date('Y-m-d');

        if($today == $t_date){
            $transaction = array();

            $doctor_id = $transaction_node->field_doctor->target_id;
            $patient_id = $transaction_node->field_patient->target_id;

            $transaction['nid'] = $nid;
            $transaction['transaction_date'] = $t_date;
            $transaction['doctor'] = Node::load($doctor_id)->getTitle();
            $transaction['doc_id'] = $doctor_id;
            $transaction['patient'] = Node::load($patient_id)->getTitle();
            $transaction['patient_code'] = Node::load($patient_id)->field_patient_code->value;
            $transaction['patient_id'] = $patient_id;
            $transaction['amount'] = $transaction_node->field_amount->value;
            $transaction['cash_paid'] = $transaction_node->field_cash_paid->value;
            $transaction['prescription_number'] = $transaction_node->field_prescription_number->value;
            $transaction['blood_pressure'] = $transaction_node->field_blood_pressure->value;
            $transaction['blood_sugar_fasting'] = $transaction_node->field_blood_sugar_fasting->value;
            $transaction['blood_sugar_pp'] = $transaction_node->field_blood_sugar_pp->value;
            $transaction['blood_sugar_random'] = $transaction_node->field_blood_sugar_random->value;
            $transaction['narration'] = $transaction_node->field_narration->value;
            $transaction['sl_no'] = $transaction_node->field_sl_no->value;
            $transaction['receipt_no'] = $transaction_node->field_receipt_no->value;

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_transaction', $nid)
                ->condition('field_status', 'u');
            $due_entity_ids = $query->execute();
            $total_due = 0;
            if (!empty($due_entity_ids)) {
                $due_nodes = Node::loadMultiple($due_entity_ids);
                foreach ($due_nodes as $due_node) {
                    $total_due += $due_node->get('field_amount_due')->value;
                }
            }

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'transaction_service')->condition('field_transaction', $nid);
            $service_ids = $query->execute();

            $data['services'] = array();
            if(!empty($service_ids)) {
                $service_nodes = Node::loadMultiple($service_ids);
                foreach ($service_nodes as $key => $service_node) {
                    $service_details = array();
                    $service_id = $service_node->get('field_service')->target_id;
                    $service  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($service_id);
                    $service = $service->toArray();
                    $service_details['id']  = $service_node->id();
                    $service_details['service_name']  = $service['name'][0]['value'];
                    $service_details['count'] = $service_node->get('field_service_count')->value;
                    $service_details['base_amount'] = $service_node->get('field_base_amount')->value;
                    $service_details['amount'] = $service_node->get('field_amount')->value;
                    array_push($data['services'],$service_details);
                }
            }

            $data['transaction'] = $transaction;
            $data['due'] = $total_due;
            $build = [
                '#theme' => 'edit_transaction_form',
                '#data' => $data
            ];
            $build['#cache']['max-age'] = 0;
            return $build;
        }

        else{
            $build = [
                '#theme' => 'edit_permission_validate',
                '#data' => $data
            ];
            $build['#cache']['max-age'] = 0;
            return $build;
        }

    }

    public function editTransactionForm()
    {
        $pass = \Drupal::request()->request->get('pass');
        $isEdit = \Drupal::request()->request->get('isEdit');

        if ($isEdit == null) {
            $editPass = $this->config('charges_slip.adminsettings')->get('editPassword');
            if ($editPass == null) {
                $response = new RedirectResponse("/password-form");
                $response->send();
            } else if (password_verify($pass, $editPass)) {
                $data = array();

                $nid = \Drupal::request()->request->get('nid');
                $transaction_node = Node::load($nid);
                $transaction = array();

                $doctor_id = $transaction_node->field_doctor->target_id;
                $patient_id = $transaction_node->field_patient->target_id;

                $transaction['nid'] = $nid;
                $transaction['transaction_date'] = $transaction_node->field_transaction_date->value;
                $transaction['doctor'] = Node::load($doctor_id)->getTitle();
                $transaction['doc_id'] = $doctor_id;
                $transaction['patient'] = Node::load($patient_id)->getTitle();
                $transaction['patient_code'] = Node::load($patient_id)->field_patient_code->value;
                $transaction['patient_id'] = $patient_id;
                $transaction['amount'] = $transaction_node->field_amount->value;
                $transaction['cash_paid'] = $transaction_node->field_cash_paid->value;
                $transaction['prescription_number'] = $transaction_node->field_prescription_number->value;
                $transaction['blood_pressure'] = $transaction_node->field_blood_pressure->value;
                $transaction['blood_sugar_fasting'] = $transaction_node->field_blood_sugar_fasting->value;
                $transaction['blood_sugar_pp'] = $transaction_node->field_blood_sugar_pp->value;
                $transaction['blood_sugar_random'] = $transaction_node->field_blood_sugar_random->value;
                $transaction['narration'] = $transaction_node->field_narration->value;
                $transaction['sl_no'] = $transaction_node->field_sl_no->value;
                $transaction['receipt_no'] = $transaction_node->field_receipt_no->value;

                $query = \Drupal::entityQuery('node');
                $query->condition('type', 'payment_due')
                    ->condition('field_transaction', $nid)
                    ->condition('field_status', 'u');
                $due_entity_ids = $query->execute();
                $total_due = 0;
                if (!empty($due_entity_ids)) {
                    $due_nodes = Node::loadMultiple($due_entity_ids);
                    foreach ($due_nodes as $due_node) {
                        $total_due += $due_node->get('field_amount_due')->value;
                    }
                }

                $query = \Drupal::entityQuery('node');
                $query->condition('type', 'transaction_service')->condition('field_transaction', $nid);
                $service_ids = $query->execute();

                $data['services'] = array();
                if(!empty($service_ids)) {
                    $service_nodes = Node::loadMultiple($service_ids);
                    foreach ($service_nodes as $key => $service_node) {
                        $service_details = array();
                        $service_id = $service_node->get('field_service')->target_id;
                        $service  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($service_id);
                        $service = $service->toArray();
                        $service_details['id']  = $service_node->id();
                        $service_details['service_name']  = $service['name'][0]['value'];
                        $service_details['count'] = $service_node->get('field_service_count')->value;
                        $service_details['base_amount'] = $service_node->get('field_base_amount')->value;
                        $service_details['amount'] = $service_node->get('field_amount')->value;
                        array_push($data['services'],$service_details);
                    }
                }

                $data['transaction'] = $transaction;
                $data['due'] = $total_due;
                $build = [
                    '#theme' => 'edit_transaction_form',
                    '#data' => $data
                ];
                $build['#cache']['max-age'] = 0;
                return $build;
            } else {
                drupal_set_message("Invalid Password", "error");
                $response = new RedirectResponse("/transaction-details");
                $response->send();
            }
        }
        if ($isEdit == 'true') {

            $pid = $_POST['patient_id'];
            $node = Node::load($_POST['nid']);
            $node->set('field_patient', $pid);
            $node->set('field_doctor', $_POST['doc']);
            $node->set('field_amount', $_POST['amount']);
            $node->set('field_cash_paid', $_POST['cash_paid']);
            $node->set('field_prescription_number', $_POST['prescription_number']);
            $node->set('field_blood_sugar_fasting', $_POST['blood_sugar_fasting']);
            $node->set('field_blood_sugar_pp', $_POST['blood_sugar_pp']);
            $node->set('field_blood_sugar_random', $_POST['blood_sugar_random']);
            $node->set('field_blood_pressure', $_POST['blood_pressure']);
            $node->set('field_narration', $_POST['narration']);
            $node->save();

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_transaction', $_POST['nid']);
            $due_entity_ids = $query->execute();
            $due_node = Node::load(reset($due_entity_ids));

            if($due_node){
                $due_node->set('field_patient', $pid);
                if ($_POST['due'] > 0) {
                    $due_node->set('field_amount_due', $_POST['due']);
                    $due_node->set('field_status', 'u');
                    drupal_set_message("Due Created Successfully");
                } else {
                    $due_node->set('field_amount_due', $_POST['due']);
                    $due_node->set('field_status', 'p');
                    drupal_set_message("Due Cleared");
                }
                $due_node->save();
            }
            else{
                if ($_POST['due'] > 0) {
                    $due_node = Node::create([
                        'type'=>'payment_due',
                        'field_amount_due'=> $_POST['due'],
                        'field_transaction'=>$_POST['nid'],
                        'field_patient'=>$pid,
                        'field_status'=> 'u'
                    ]);
                    $due_node->save();
                    drupal_set_message("Due Created Successfully");
                }
            }

            $services = [];
            if ($_POST['services'] != '') {
                $services = json_decode($_POST['services'], 1);
            }
            foreach ($services as $service){
                if(sizeof($service)>0){
                    $node = Node::load($service['nid']);
                    $node->set('field_base_amount',$service['base_amount']);
                    $node->set('field_service_count',$service['count']);
                    $node->set('field_amount',$service['amount']);
                    $node->save();

                }
            }

            drupal_set_message("Transaction Updated Successfully.");
            $response = new RedirectResponse("/transaction-details");
            $response->send();
        }
        return [];
    }

    public function checkWhitelist(){

        $data = array();
        $nid = \Drupal::request()->query->get('nid');
        $data['nid'] = $nid;
        $build = [
            '#theme' => 'check_whitelist',
            '#data' => $data
        ];
        $build['#cache']['max-age'] = 0;
        return $build;

    }

    public function whitelist(){

        $pass = \Drupal::request()->request->get('pass');
        $nid = \Drupal::request()->request->get('nid');

        $editPass = $this->config('charges_slip.adminsettings')->get('editPassword');
        if ($editPass == null) {
            $response = new RedirectResponse("/password-form");
            $response->send();
        } else{

            if (password_verify($pass, $editPass)){

                $due_entity = Node::load($nid);
                if($due_entity->field_status->value == 'u'){

                    $tid = $due_entity->field_transaction->target_id;
                    $due_amount = $due_entity->field_amount_due->value;

                    $transaction_entity = Node::load($tid);
                    $old_amount = $transaction_entity->field_amount->value;
                    $new_amt = $old_amount - $due_amount;

                    if($new_amt >= 0){
                        $transaction_entity->set('field_amount',$new_amt);
                        $transaction_entity->save();

                        $due_entity->set('field_status','w');
                        $due_entity->save();
                    }
                    else{
                        drupal_set_message("Sorry, Cannot Whitelist this Due.", "error");
                        $response = new RedirectResponse("/payment-due");
                        $response->send();
                    }

                    drupal_set_message("Due Whitelisted successfully","status");
                    $response = new RedirectResponse("/payment-due");
                    $response->send();

                }

            }
            else{
                drupal_set_message("Invalid Password", "error");
                $response = new RedirectResponse("/payment-due");
                $response->send();
            }

        }

//        $build['#cache']['max-age'] = 0;
//        return $build;

    }

}
