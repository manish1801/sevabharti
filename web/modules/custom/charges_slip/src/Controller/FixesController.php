<?php

namespace Drupal\charges_slip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\node\Entity\Node;

/**
 * Class FixesController.
 */
class FixesController extends ControllerBase
{
    public function generateServiceCount()
    {
        $nids = \Drupal::entityQuery('node')->condition('type','transaction_service')->notExists('field_service_count')->execute();
        foreach ($nids  as $nid){
            $node = Node::load($nid);
            $amt = $node->field_amount->value;
            $node->set('field_base_amount',$amt);
            $node->set('field_service_count',1);
            try {
                $node->save();
            } catch (EntityStorageException $e) {}
        }
        $build = [];
        $build['#cache']['max-age'] = 0;
        return $build;
    }

    public function generateCashPaid()
    {
        $nids = \Drupal::entityQuery('node')->condition('type','transaction')->notExists('field_cash_paid')->execute();
        foreach ($nids  as $nid){

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'transaction_service')->condition('field_transaction', $nid);
            $services = $query->execute();

            $total = 0;
            foreach ($services as $sid){
                $s = Node::load($sid);
                $total += $s->field_amount->value;
            }

            $node = Node::load($nid);
            $amt = $node->field_amount->value;
            $node->set('field_cash_paid',$amt);
            $node->set('field_amount',$total);
            try {
                $node->save();
            } catch (EntityStorageException $e) {}
        }
        $build = [];
        $build['#cache']['max-age'] = 0;
        return $build;
    }

}
