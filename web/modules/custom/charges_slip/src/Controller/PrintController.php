<?php

namespace Drupal\charges_slip\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\HttpFoundation\RedirectResponse;


/**
 * Class PrintController.
 */
class PrintController extends ControllerBase {
	public function printDoctorConsultant() {

        $response = new RedirectResponse('/slip');

	    $tid = \Drupal::request()->get('id');

	    if($tid == null)
            $response->send();

		$transaction_data = Node::load($tid);

        if($transaction_data == null)
            $response->send();

        $serial_no = $transaction_data->get('field_sl_no')->value;
        $receipt_no = $transaction_data->get('field_receipt_no')->value;

		$transaction_data = $transaction_data->toArray();

		$doctor_node_id  = $transaction_data['field_doctor'][0]['target_id']; //$doctor_node_id  = $transaction_data->field_doctor->target_id;
		$patient_node_id = $transaction_data['field_patient'][0]['target_id'];

        if($doctor_node_id == null || $patient_node_id == null)
            $response->send();

		$doctor_data = Node::load($doctor_node_id);
		$doctor_data = $doctor_data->toArray();

		$doc_dept = $doctor_data['field_department'][0]['target_id'];
        $doc_dept = Term::load($doc_dept);
        $doc_dept = $doc_dept->getName();

		$patient_data = Node::load($patient_node_id);
		$patient_data = $patient_data->toArray();

		$query = \Drupal::entityQuery('node');
		$query->condition('type', 'payment_due')
			->condition('field_transaction', $transaction_data['nid'][0]['value']);
//			->condition('field_status', 'u');
		$due_entity_ids = $query->execute();
		$total_due = 0;
		if(!empty($due_entity_ids)) {
			$due_nodes = Node::loadMultiple($due_entity_ids);
			foreach ($due_nodes as $due_node) {
				$total_due += $due_node->get('field_amount_due')->value;
			}
		}

		$query = \Drupal::entityQuery('node');
		$query->condition('type', 'transaction_service')
			->condition('field_transaction', $transaction_data['nid'][0]['value']);
		$transaction_service_entity_ids = $query->execute();
		$transaction_service_nodes = array();
		$service_node_data = array();
		$total_service_amount=0;
		if(!empty($transaction_service_entity_ids)) {
			$transaction_service_nodes = Node::loadMultiple($transaction_service_entity_ids);
			//$transaction_service_nodes = $transaction_service_nodes->toArray();
			foreach ($transaction_service_nodes as $key => $transaction_service_node) {
				$service_id = $transaction_service_node->get('field_service')->target_id;
				$service  = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($service_id);
				$service = $service->toArray();
				$service_node_data[$key]['service_name']  = $service['name'][0]['value'];
				$service_node_data[$key]['amount'] = $transaction_service_node->get('field_amount')->value;
                $total_service_amount = $total_service_amount + $transaction_service_node->get('field_amount')->value;
			}
		}


		$data = array();
		$data['patient'] = $patient_data;
		$data['doctor']  = $doctor_data;
		$data['doctor_dept']  = $doc_dept;
		$data['transaction'] = $transaction_data;
		$data['total_due'] = $total_due;
		$data['transaction_services'] = array_reverse($service_node_data);
		$data['serial_no'] = $serial_no;
		$data['receipt_no'] = $receipt_no;
		$data['total_service_amount'] = $total_service_amount;

        $build = [
            '#theme' => 'print_page',
            '#data' => $data,
            '#attached' => [
                'library' => [
                    'charges_slip/sevabharti-styles', //include our custom library for this response
                ]
            ]
        ];

        $build['#cache']['max-age'] = 0;

        return $build;

	}
}
