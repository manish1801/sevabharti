<?php

namespace Drupal\charges_slip\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\node\Entity\Node;

/**
 * Class ChargesSlipForm.
 */
class ChargesSlipForm extends FormBase
{


    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'charges_slip_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['patient_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('PATIENT ID'),
            '#ajax' => array(
                'callback' => '::getPatientInformation',
                'effect' => 'fade',
                'event' => 'change',
                'progress' => array(
                    'type' => 'throbber',
                    'message' => NULL,
                ),
            ),
        ];
        $form['container'] = array(
            '#type' => 'container',
            '#prefix' => '<div id="patient_id">',
            '#suffix' => '</div>'

        );
        $form['container']['patient_name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('PATIENT NAME'),

        ];
        $form['container']['patient_code'] = [
            '#type' => 'textfield',
            '#title' => $this->t('PATIENT CODE'),
        ];
        $form['container']['patient_address'] = [
            '#type' => 'textarea',
            '#title' => $this->t('PATIENT ADDRESS'),
        ];
        $form['container']['phone_mobile'] = [
            '#type' => 'tel',
            '#title' => $this->t('PHONE/MOBILE'),
        ];
        $form['container']['age'] = [
            '#type' => 'number',
            '#title' => $this->t('AGE'),
        ];
        $form['container']['sex'] = [
            '#type' => 'select',
            '#options' => array(
                '_none' => t('- Select a value -'),
                'male' => t('Male'),
                'female' => t('Female'),
            ),
            '#title' => $this->t('SEX'),
        ];
        $form['container']['old_due_amount'] = [
            '#type' => 'number',
            '#title' => $this->t('OLD DUE AMOUNT'),
            '#attributes' => array('readonly' => 'readonly'),
        ];
        $form['v_no'] = [
            '#type' => 'textfield',
            '#title' => $this->t('V. No.'),
        ];
        $form['slip_no'] = [
            '#type' => 'textfield',
            '#title' => $this->t('SLIP NO'),
        ];
        $form['department'] = [
            '#title' => $this->t('DEPARTMENT'),
            '#type' => 'entity_autocomplete',
            '#target_type' => 'taxonomy_term',
            '#autocreate' => array(
                'bundle' => 'department',
            ),
        ];
        $form['date'] = [
            '#type' => 'date',
            '#title' => $this->t('DATE'),
        ];
        $form['prescription_no'] = [
            '#type' => 'textfield',
            '#title' => $this->t('PRESCRIPTION NO'),
        ];
        $form['doctor_name'] = array(
            '#title' => $this->t('DOCTOR\'s NAME'),
            '#type' => 'entity_autocomplete',
            '#target_type' => 'node',
            '#selection_handler' => 'default',
            '#selection_settings' => array(
                'target_bundles' => array('node', 'doctor'),
            ),
        );
        $form['doctor_amount'] = [
            '#type' => 'number',
            '#title' => $this->t('DOCTOR\'s AMOUNT'),
            '#ajax' => array(
                'callback' => '::getTotalAmount',
                'effect' => 'fade',
                'event' => 'change',
                'progress' => array(
                    'type' => 'throbber',
                    'message' => NULL,
                ),
            ),
        ];
        $form['amount_container'] = array(
            '#type' => 'container',
            '#prefix' => '<div id="amount_id">',
            '#suffix' => '</div>'

        );
        $form['amount_container']['total'] = [
            '#type' => 'number',
            '#title' => $this->t('TOTAL'),
            '#attributes' => array('readonly' => 'readonly'),
        ];
        $form['cash_paid'] = [
            '#type' => 'number',
            '#title' => $this->t('CASH PAID'),
            '#ajax' => array(
                'callback' => '::getDueAmount',
                'effect' => 'fade',
                'event' => 'change',
                'progress' => array(
                    'type' => 'throbber',
                    'message' => NULL,
                ),
            ),
        ];
        $form['due_container'] = array(
            '#type' => 'container',
            '#prefix' => '<div id="due_id">',
            '#suffix' => '</div>'

        );
        $form['due_container']['due'] = [
            '#type' => 'number',
            '#title' => $this->t('DUE'),
            '#attributes' => array('readonly' => 'readonly'),
        ];
        $form['narration'] = [
            '#type' => 'textfield',
            '#title' => $this->t('NARRATION'),
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $patientId = $form_state->getValue('patient_id');
        $patientDetails = Node::load($patientId);

        $pid = -1;

        if ($patientDetails == NULL) {

            $patientName = $form_state->getValue('patient_name');
            $patientAdd = $form_state->getValue('patient_address');
            $patientPh = $form_state->getValue('phone_mobile');
            $patientAge = $form_state->getValue('age');
            $patientSex = $form_state->getValue('sex');
            $patientCode = $form_state->getValue('patient_code');

            $node = Node::create([
                'type' => 'patient',
                'title' => $patientName,
                'field_address' => $patientAdd,
                'field_age' => $patientAge,
                'field_phone_number' => $patientPh,
                'field_sex' => $patientSex,
                'field_patient_code' => $patientCode,
            ]);
            $node->save();

            $pid = $node->id();

            drupal_set_message('Patient created Successfully');

        }

        if ($pid == -1)
            $pid = $form_state->getValue('patient_id');

        $node = Node::create([
            'type' => 'transaction',
            'title' => 'Sample Transaction',
            'field_prescription_number' => $form_state->getValue('prescription_no'),
            'field_slip_number' => $form_state->getValue('slip_no'),
            'field_v_number' => $form_state->getValue('v_no'),
            'field_doctor' => $form_state->getValue('doctor_name'),
            'field_patient' => $pid,
            'field_amount' => $form_state->getValue('cash_paid'),
            'field_transaction_type' => 'Doctor\'s Consultant',
        ]);
        $node->save();

        $tid = $node->id();

        if ($form_state->getValue('due') > 0) {

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_patient', $pid)
                ->condition('field_status', 'u');
            $entity_ids = $query->execute();

            if (sizeof($entity_ids) > 0) {
                $did = reset($entity_ids);
                $due = Node::load($did);
                $due->field_status->value = 'p';
                $due->save();
                $node = Node::create([
                    'type' => 'payment_due',
                    'field_amount_due' => $form_state->getValue('due'),
                    'field_patient' => $pid,
                    'field_transaction' => $tid,
                ]);
                $node->save();
                drupal_set_message('Payment Due Added');
            }
            else{
                $node = Node::create([
                    'type' => 'payment_due',
                    'field_amount_due' => $form_state->getValue('due'),
                    'field_patient' => $pid,
                    'field_status' => 'u',
                    'field_transaction' => $tid,
                ]);
                $node->save();
                drupal_set_message('Payment Due Added');
            }
        }

        if ($form_state->getValue('due') == "" || $form_state->getValue('due') == "0") {


            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_patient', $pid)
                ->condition('field_status', 'u');
            $entity_ids = $query->execute();

            if (sizeof($entity_ids) > 0) {
                $did = reset($entity_ids);
                $due = Node::load($did);
                $due->field_status->value = 'p';
                $due->save();
                drupal_set_message('Payment Due Cleared');
            }
        }

        drupal_set_message('Transaction Created Successfully');
//				$form_state->setRedirect('machine_name');
				return;

    }

    public function getPatientInformation(array $form, FormStateInterface $form_state)
    {
        $ajaxResponse = new AjaxResponse();
        $patientId = $form_state->getValue('patient_id');
        $patientDetails = Node::load($patientId);

        if ($patientDetails != NULL) {

            $query = \Drupal::entityQuery('node');
            $query->condition('type', 'payment_due')
                ->condition('field_patient', $patientId)
                ->condition('field_status', 'u');
            $entity_ids = $query->execute();

            $did = reset($entity_ids);
            $due = Node::load($did);
            $dueAmount = $due->field_amount_due->value;

            $form['container']['old_due_amount']['#value'] = $dueAmount;

            $patientName = $patientDetails->getTitle();
            $patientAddress = $patientDetails->field_address->value;
            $patientPhoneNumber = $patientDetails->field_phone_number->value;
            $patientAge = $patientDetails->field_age->value;
            $patientSex = $patientDetails->field_sex->value;
            $patientCode = $patientDetails->field_patient_code->value;
            if (isset($patientName) AND !empty($patientName)) {
                $form['container']['patient_name']['#value'] = $patientName;
            }
            if (isset($patientAddress) AND !empty($patientAddress)) {
                $form['container']['patient_address']['#value'] = $patientAddress;
            }
            if (isset($patientPhoneNumber) AND !empty($patientPhoneNumber)) {
                $form['container']['phone_mobile']['#value'] = $patientPhoneNumber;
            }
            if (isset($patientAge)) {
                $form['container']['age']['#value'] = $patientAge;
            }
            if (isset($patientCode)) {
                $form['container']['patient_code']['#value'] = $patientCode;
            }
            if (isset($patientSex)) {
                $form['container']['sex']['#options'] = array(strtolower($patientSex) => $patientSex);
            }
            $ajaxResponse->addCommand(new HtmlCommand('#patient_id', $form['container']));

        }

        return $ajaxResponse;
    }

    public function getTotalAmount(array $form, FormStateInterface $form_state)
    {
        $ajaxResponse = new AjaxResponse();

        $amt =  (int)$form_state->getValue('doctor_amount') + (int)$form_state->getValue('old_due_amount');

        $form['amount_container']['total']['#value'] = $amt;

        $ajaxResponse->addCommand(new HtmlCommand('#amount_id', $form['amount_container']));
        return $ajaxResponse;
    }

    public function getDueAmount(array $form, FormStateInterface $form_state)
    {
        $ajaxResponse = new AjaxResponse();

        $amt =  (int)$form_state->getValue('total') - (int)$form_state->getValue('cash_paid');

        $form['due_container']['due']['#value'] = $amt;

        $ajaxResponse->addCommand(new HtmlCommand('#due_id', $form['due_container']));
        return $ajaxResponse;
    }
}
