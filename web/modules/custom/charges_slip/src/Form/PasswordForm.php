<?php

namespace Drupal\charges_slip\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PasswordForm.
 */
class PasswordForm extends ConfigFormBase
{


    protected function getEditableConfigNames() {
        return [
            'charges_slip.adminsettings',
        ];
    }

    public function getFormId()
    {
        return 'password_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $editPass = $this->config('charges_slip.adminsettings')->get('editPassword');

        $a = 1;

        if($editPass != null){
            $form['cur_pass'] = [
                '#title'=>t('Current Password'),
                '#type' => 'password',
                '#required' => true,
                '#placeholder' => t('Current Password'),
            ];
        }

        $form['new_pass'] = [
            '#type' => 'password_confirm',
            '#required' => true,
            '#placeholder' => t('New Password'),
        ];

        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit')
        ];

        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {

        $errors = array();

        $editPass = $this->config('charges_slip.adminsettings')->get('editPassword');
        $cur_pass = $form_state->getValue('cur_pass');

        if($editPass != null){
            if(!password_verify($cur_pass,$editPass)){
                $msg = 'Invalid Current Password';
                $errors[] = ['Invalid Current Password' => $msg];
            }

            if (!empty($errors)) {
                $ers = '';
                foreach ($errors as $errors_key => $errors_value) {
                    foreach ($errors_value as $item) {
                        $ers .= $item . '<br>';
                    }
                }
                $form_state->setError($form, t($ers));
            }
        }

    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $new_pass = $form_state->getValue('new_pass');

        $editPass = $this->config('charges_slip.adminsettings')->get('editPassword');
        if($editPass)
            $msg = "Password Changed Successfully";
        else
            $msg = "Password Created Successfully";

        $hashPass = password_hash($new_pass,PASSWORD_DEFAULT);

        $this->config('charges_slip.adminsettings')->set('editPassword',$hashPass)->save();
//        $this->config('charges_slip.adminsettings')->delete();

        drupal_set_message($msg);
        $response = new RedirectResponse("/");
        $response->send();

    }

}
